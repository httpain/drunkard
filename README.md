# Drunkard
Drunkard is a local variation of well-known [card game War](https://en.wikipedia.org/wiki/War_%28card_game%29).

## Example of command-line output

```
---Round started
Player 0 opens jack of clubs
Player 1 opens six of spades
Player 2 opens five of hearts
Player 3 opens ten of spades
The winner is Player 0 and he gets:
 - jack of clubs
 - six of spades
 - five of hearts
 - ten of spades
After round Player 0 has 15 cards
After round Player 1 has 11 cards
After round Player 2 has 11 cards
After round Player 3 has 11 cards

---Round started
Player 0 opens ten of diamonds
Player 1 opens three of spades
Player 2 opens eight of spades
Player 3 opens two of clubs
The winner is Player 0 and he gets:
 - ten of diamonds
 - three of spades
 - eight of spades
 - two of clubs
After round Player 0 has 18 cards
After round Player 1 has 10 cards
After round Player 2 has 10 cards
After round Player 3 has 10 cards

---Round started
Player 0 opens jack of diamonds
Player 1 opens six of clubs
Player 2 opens four of clubs
Player 3 opens two of spades
The winner is Player 0 and he gets:
 - jack of diamonds
 - six of clubs
 - four of clubs
 - two of spades
After round Player 0 has 21 cards
After round Player 1 has 9 cards
After round Player 2 has 9 cards
After round Player 3 has 9 cards

---Round started
Player 0 opens five of clubs
Player 1 opens seven of hearts
Player 2 opens nine of diamonds
Player 3 opens ten of clubs
The winner is Player 3 and he gets:
 - five of clubs
 - seven of hearts
 - nine of diamonds
 - ten of clubs
After round Player 0 has 20 cards
After round Player 1 has 8 cards
After round Player 2 has 8 cards
After round Player 3 has 12 cards

---Round started
Player 0 opens king of hearts
Player 1 opens two of hearts
Player 2 opens king of diamonds
Player 3 opens eight of clubs
Player 0 opens queen of spades
Player 2 opens three of hearts
The winner is Player 0 and he gets:
 - queen of spades
 - king of hearts
 - two of hearts
 - three of hearts
 - king of diamonds
 - eight of clubs
After round Player 0 has 24 cards
After round Player 2 has 6 cards

---Round started
Player 0 opens five of spades
Player 1 opens jack of spades
Player 2 opens jack of hearts
Player 3 opens nine of hearts
Player 1 opens queen of diamonds
Player 2 opens queen of hearts
Player 1 opens nine of spades
Player 2 opens six of diamonds
The winner is Player 1 and he gets:
 - five of spades
 - nine of spades
 - queen of diamonds
 - jack of spades
 - six of diamonds
 - queen of hearts
 - jack of hearts
 - nine of hearts
After round Player 1 has 12 cards
After round Player 2 has 3 cards

---Round started
Player 0 opens king of spades
Player 1 opens nine of clubs
Player 2 opens four of hearts
Player 3 opens four of diamonds
The winner is Player 0 and he gets:
 - king of spades
 - nine of clubs
 - four of hearts
 - four of diamonds
After round Player 0 has 26 cards
After round Player 1 has 11 cards
After round Player 2 has 2 cards
After round Player 3 has 9 cards

---Round started
Player 0 opens ten of hearts
Player 1 opens seven of diamonds
Player 2 opens four of spades
Player 3 opens eight of hearts
The winner is Player 0 and he gets:
 - ten of hearts
 - seven of diamonds
 - four of spades
 - eight of hearts
After round Player 0 has 29 cards
After round Player 1 has 10 cards
After round Player 2 has 1 cards
After round Player 3 has 8 cards

---Round started
Player 0 opens eight of diamonds
Player 1 opens king of clubs
Player 2 opens five of diamonds
Player 3 opens six of hearts
The winner is Player 1 and he gets:
 - eight of diamonds
 - king of clubs
 - five of diamonds
 - six of hearts
After round Player 0 has 28 cards
After round Player 1 has 13 cards
After round Player 2 has 0 cards
After round Player 3 has 7 cards

---Round started
Player 0 opens two of diamonds
Player 1 opens three of diamonds
Player 2 has 0 cards and lost
```